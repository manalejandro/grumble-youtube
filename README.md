# grumble-youtube

## Install

`npm i` or `yarn`

## Usage

```
$ MUMBLE_SERVER=server.domain.com node index.js
```

## Play

```
Write in grumble: play <youtube-link>

Example:
play https://www.youtube.com/watch?v=XXXXXXX

You can use other links sites such as youtube-dl software
```

## License

### MIT