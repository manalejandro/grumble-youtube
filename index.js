const { NodeGrumble, Events, MessageType } = require('node-grumble'),
    ytdl = require('ytdl-core'),
    ytdlcore = require('youtube-dl'),
    grumble = NodeGrumble.create({
        url: String(process.env.MUMBLE_SERVER),
        name: String('grumble-youtube'),
    }),
    volume = 0.6

console.log('Connecting');
(async () => {
    grumble.on(Events.Connected, () => {
        console.log('Connected to ' + process.env.MUMBLE_SERVER)
    })
    const connection = await grumble.connect()
    grumble.on(MessageType.TextMessage, message => {
        const msg = message.message.replace(/<[^>]*>?/gm, '')
        if (msg.split(' ')[0] === 'play' && msg.split(' ').length === 2) {
            if (msg.split(' ')[1].match(/^https?:\/\/www\.youtube\.com\/watch\?v=/i)) {
                connection.playFile(ytdl(msg.split(' ')[1], { filter: 'audioonly' }), volume)
            } else if (msg.split(' ')[1].match(/^https?:\/\//i)) {
                connection.playFile(ytdlcore(msg.split(' ')[1], ['-x']), volume)
            }
            connection.sendTextMessage('Playing... ' + msg.split(' ')[1])
        }
    })
})()